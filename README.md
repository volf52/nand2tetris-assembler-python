# Assembler - Hack ASM

[![DeepSource](https://static.deepsource.io/deepsource-badge-light-mini.svg)](https://deepsource.io/gh/volf52/nand2tetris-assembler-python/?ref=repository-badge)

[Project 6](https://www.nand2tetris.org/project06) for the [Nand2Tetris](https://www.nand2tetris.org/) course(?)
